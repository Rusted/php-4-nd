<?php

$countries = ["Italy" => "Rome", "Luxembourg" => "Luxembourg", "Belgium" => "Brussels",
    "Denmark" => "Copenhagen", "Finland" => "Helsinki", "France" => "Paris",
    "Slovakia" => "Bratislava", "Slovenia" => "Ljubljana", "Germany" => "Berlin", "Greece" => "Athens",
    "Ireland" => "Dublin", "Netherlands" => "Amsterdam", "Portugal" => "Lisbon", "Spain" => "Madrid",
    "Sweden" => "Stockholm", "United Kingdom" => "London", "Cyprus" => "Nicosia",
    "Lithuania" => "Vilnius", "Czech Republic" => "Prague", "Estonia" => "Tallin",
    "Hungary" => "Budapest", "Latvia" => "Riga", "Malta" => "Valetta", "Austria" => "Vienna",
    "Poland" => "Warsaw"];
?>
<h3>Pagal sostines didėjimo tvarka</h3>
<p>
<?php
asort($countries);
foreach ($countries as $country => $capitol) {
    echo "The capitol of $country is $capitol.<br>";
}
?>
</p>
<h3>Pagal šalis mažėjimo tvarka</h3>
<p>
<?php
krsort($countries);
foreach ($countries as $country => $capitol) {
    echo "The capitol of $country is $capitol.<br>";
}
?>
</p>