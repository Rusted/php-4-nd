<html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
        crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</head>

<body>
    <div class="container">
        <h1>Temperatūrų skaičiuoklė</h1>
        <?php if (!empty($_POST['temperatures'])) {?>
        <p class="well">
    <?php
$temperatures = array_map('intval', explode(',', $_POST['temperatures']));
    $average = round(array_sum($temperatures) / count($temperatures), 2);
    sort($temperatures);
    if (count($temperatures <= 2)) {
        $lowest = $temperatures;
        $highest = $temperatures;
    } else {
        $lowest = array_slice($array, 0, 2);
        $highest = array_slice($array, -2);
    }
    ?>
            Vidutinės temperatūros laipsniai: <?php echo $average; ?><br>
            Viso nuoskaitų: <?php echo count($temperatures); ?><br>
            Dvi didžiausios temperatūros: <?php echo implode(', ', $highest); ?><br>
            Dvi mažiausios temperatūros: <?php echo implode(', ', $lowest); ?>
            </p>
        <?php }?>
        <form class="row-border" method="POST">
            <div class="form-group">
                <label class="control-label col-md-12">Įveskite temperatūras atskirtas kableliu</label>
                <div class="col-md-12">
                    <input type="text" class="form-control" name="temperatures" placeholder="20,21,22" value="<?php echo $_POST['temperatures']; ?>" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label col-md-12"></label>
                    <input type="submit" value="Siųsti" class="btn btn-primary" />
                </div>
            </div>
        </form>
    </div>
</body>

</html>
